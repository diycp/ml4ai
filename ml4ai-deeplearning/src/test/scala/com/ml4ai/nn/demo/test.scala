package com.ml4ai.nn.demo

import akka.actor.Actor
import org.junit.Test

class TestContainer {

  implicit def str2JavaScriptWrapper(str: String) = new JavaScriptWrapper(str)

  implicit def str2TypeScriptWrapper(str: String) = new TypeScriptWrapper(str)

  @Test
  def test(): Unit = {
    val a = "";
    a.javaScript()
    a.typeScript()
  }

}

class JavaScriptWrapper(var value: String) {

  def javaScript(): Unit = {
    new AdministratorActor().sender() ! "aaa"
  }

}

class AdministratorActor extends Actor {

  override def receive: Receive = {
    case "" => {

    }
  }

}

class TypeScriptWrapper(var value: String) {

  def typeScript(): Unit = {

  }

}
