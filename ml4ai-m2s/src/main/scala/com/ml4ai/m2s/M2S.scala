package com.ml4ai.m2s

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration

object M2S {

  def main(args: Array[String]): Unit = {
    val app = new SpringApplication(classOf[M2S])
    app.run(args: _*)
  }

}

@SpringBootApplication(exclude = Array(classOf[DataSourceAutoConfiguration], classOf[HibernateJpaAutoConfiguration]))
class M2S {

}