import { NgModule } from '@angular/core';
import { AppHeaderComponent } from './app-header/app-header';
import { BrowserModule } from '@angular/platform-browser';
import { IonicPageModule } from 'ionic-angular';
import { AppAbstractsComponent } from './app-abstracts/app-abstracts';
@NgModule({
	declarations: [
		AppHeaderComponent,
		AppAbstractsComponent
	],
	imports: [
		BrowserModule,
		IonicPageModule
	],
	exports: [
		AppHeaderComponent,
		AppAbstractsComponent
	]
})
export class ComponentsModule { }
