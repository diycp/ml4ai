import { Component } from '@angular/core';

/**
 * Generated class for the AppHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'app-header',
  templateUrl: 'app-header.html'
})
export class AppHeaderComponent {

  title: string = "自然科学物理数学研究";

  constructor() {
    console.log('Hello AppHeaderComponent Component');
  }

}
