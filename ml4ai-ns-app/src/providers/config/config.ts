import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { App } from '../../app/app.component';
import { BootPage } from '../../pages/boot/boot';

/*
  Generated class for the ConfigProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConfigProvider {

  public messageQueueWebSocketAddress: string = 'ws://www.ml4ai.com:15674/ws';
  public messageQueueUsername: string = 'app';
  public messageQueuePassword: string = 'app';
  public messageQueueVirtualhost: string = '/app';

  public root: BootPage;

  constructor() {
  }

}
