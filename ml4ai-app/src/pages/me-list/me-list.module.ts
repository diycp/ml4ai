import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeListPage } from './me-list';

@NgModule({
  declarations: [
    MeListPage,
  ],
  imports: [
    IonicPageModule.forChild(MeListPage),
  ],
})
export class MeListPageModule {}
