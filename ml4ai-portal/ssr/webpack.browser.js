const path = require('path')
const HtmlWebpackPlugin = require("html-webpack-plugin")
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

module.exports = {
    entry: {
        app: path.join(__dirname, './client/client-entry.js')
    },
    output: {
        filename: 'client-entry.js',
        path: path.join(__dirname, './dist'),
        publicPath: ""
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: "babel-loader",
                    options: {
                        "presets": [
                            "env",
                            "react",
                            "es2015",
                            "stage-2",
                        ]
                    }
                },
                exclude: [
                    path.join(__dirname, "../node_modules")
                ],

            }, {
                test: /\.html$/,
                use: {
                    loader: "html-loader",
                    options: { minimize: false }
                }
            },
            {
                test: /\.(s)?css$/,
                exclude: /node_modules/,
                loader: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "postcss-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.(png|jpg|gif|woff|svg|eot|woff2|tff|svg)$/,
                use: 'url-loader?limit=8129',
                exclude: /node_modules/
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'React',
            template: path.resolve(__dirname, './public/index.html'),
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css'
        })
    ]
}