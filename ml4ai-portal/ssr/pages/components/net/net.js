import axios from 'axios'

const prefix = "http://www.ml4ai.com/"

export default class NetProvider {

    postJson(url, data, success, error, context) {
        axios.post(prefix + url, data, {
            'Content-type': "application/json;charset=utf-8"
        }).then((response) => {
            if (success) {
                success(response)
            }
        }).catch(errorRes => {
            if (error) {
                error(errorRes)
            }
        })
    }

    get(url, success, error, context) {
        axios.get(prefix + url).then((response) => {
            if (success) {
                success(response)
            }
        }).catch(errorRes => {
            if (error) {
                error(errorRes)
            }
        })
    }

}