import React from 'react'
import Markdown from '../fragments/markdown'
import { Menu } from 'antd'
import { Horizontal, West, East, Center, Vertical, South, North, Div } from '../fragments/layout'
import { connect } from 'react-redux'

import Fragment from '../fragments/fragment'
import Player from '../fragments/player'

const MenuItem = Menu.Item
const SubMenu = Menu.SubMenu
const Component = React.Component

class RootPage extends Component {

    constructor(props) {
        super(props)
    }

    select(me, menuItem) {
        me.props.assignPage(menuItem.key)
    }

    content(url) {
        if (url.endsWith(".md")) {
            return <div className="scrolly width100height100" >
                <div className="height100 content_lr_100">
                    <div className="padding10 ">
                        <Markdown url={url}></Markdown>
                    </div>
                </div>
            </div>
        } else if (url.endsWith(".html")) {
            return <div className="scrolly width100height100" >
                <div className="height100 content_lr_100">
                    <div className="padding10 ">
                        <Fragment url={url}></Fragment>
                    </div>
                </div>
            </div>
        } else if (url.endsWith(".flv")) {
            return <div className="scrolly width100height100" >
                <div className="height100 content_lr_100">
                    <div className="padding10 ">
                        <Player></Player>
                    </div>
                </div>
            </div>
        } else if (url.startsWith("http://") || url.startsWith("https://")) {
            return <div className="overflowHidden width100height100" >
                <iframe src={url} style={{ width: "100%", height: "100%", border: "none" }}></iframe>
            </div>
        }
    }

    render() {
        return (
            <Vertical>
                <North height={48}>
                    <Div style={{ backgroundColor: "#e7e7e7", width: "100%", height: "100%" }}>
                        <div className="content_lr_100 root_menu">
                            <Horizontal>
                                <West width={48}>
                                    <img width="48" height="48" src="assets/images/head.jpg"></img>
                                </West>
                                <Center>
                                    <Menu onClick={(item) => { this.select(this, item) }} mode="horizontal">
                                        <MenuItem key="assets/mdfiles/index.md">首页</MenuItem>
                                        <MenuItem key="assets/mdfiles/about.md">简介</MenuItem>
                                        <SubMenu title="大数据">
                                            <MenuItem key="assets/mdfiles/ld/ld.md">大数据</MenuItem>
                                            <MenuItem key="https://hadoop.apache.org">Hadoop</MenuItem>
                                            <MenuItem key="https://baike.baidu.com/item/hdfs/4836121?fr=aladdin">HDFS</MenuItem>
                                            <MenuItem key="https://baike.baidu.com/item/MapReduce%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8F">Mapreduce</MenuItem>
                                            <MenuItem key="https://spark.apache.org">Spark</MenuItem>
                                        </SubMenu>
                                        <SubMenu title="人工智能">
                                            <SubMenu title="机器学习">
                                                <MenuItem key="assets/mdfiles/machinelearning/classification.md">分类</MenuItem>
                                                <MenuItem key="https://baike.baidu.com/item/%E5%9B%9E%E5%BD%92%E5%88%86%E6%9E%90%E6%B3%95">回归</MenuItem>
                                                <MenuItem key="assets/mdfiles/machinelearning/clustering.md">聚类</MenuItem>
                                            </SubMenu>

                                            <SubMenu title="深度学习">
                                                <MenuItem key="assets/mdfiles/dl/nn.md">神经网络</MenuItem>
                                                <MenuItem key="https://baike.baidu.com/item/%E5%85%A8%E8%BF%9E%E6%8E%A5%E5%B1%82">全连接</MenuItem>
                                                <MenuItem key="assets/mdfiles/dl/bp.md">BP神经网络</MenuItem>
                                                <SubMenu title="工具箱">
                                                    <MenuItem key="assets/mdfiles/dl/tensorflow.md">Tensorflow</MenuItem>
                                                </SubMenu>
                                            </SubMenu>
                                        </SubMenu>
                                        <SubMenu title="数学">
                                            <MenuItem key="assets/mdfiles/m/l.md">线性代数</MenuItem>
                                            <MenuItem key="https://baike.baidu.com/item/%E7%BB%9F%E8%AE%A1%E5%AD%A6/1175">统计</MenuItem>
                                            <MenuItem key="https://baike.baidu.com/item/%E6%A6%82%E7%8E%87%E8%AE%BA/829122">概率</MenuItem>
                                            <MenuItem key="assets/mdfiles/m/calculus.md">微积分</MenuItem>
                                        </SubMenu>
                                    </Menu>
                                </Center>
                            </Horizontal>
                        </div>
                    </Div>
                </North>
                <Center>
                    {this.content(this.props.page ? this.props.page : "assets/mdfiles/index.md")}
                </Center>
                <South style={{ backgroundColor: "#e7e7e7" }} height={120}>
                    <div className="content_lr_100">
                        <div className="padding10">
                            <Markdown url="assets/mdfiles/content/content-foot.md"></Markdown>
                        </div>
                    </div>
                </South>
            </Vertical>
        )
    }
}

const mapState2Props = function (state) {
    return { ...state }
}

const mapDispatch2Props = function (dispatch) {
    return {
        assignPage: (url) => {
            console.log(`切换到URL：${url}`)
            dispatch({
                type: "update",
                state: {
                    page: url
                }
            })
        }
    }
}

export default connect(mapState2Props, mapDispatch2Props)(RootPage)

