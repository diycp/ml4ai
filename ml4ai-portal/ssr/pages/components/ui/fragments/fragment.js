import React from 'react'
import NetProvider from '../../net/net'

import { connect } from 'react-redux'

const Component = React.Component;

class Fragment extends Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.pull(this.props.url)
    }

    componentDidMount() {
        this.props.pull(this.props.url)
    }

    componentWillReceiveProps(props) {
        props.pull(props.url)
    }

    render() {
        return <div dangerouslySetInnerHTML={
            {
                __html: this.props[this.props.url]
            }
        }></div >;
    }

}

let mapState2PropsMarkDown = (state) => {
    return { ...state }
}

let mapDispatch2PropsMarkDown = (dispatch) => {
    return {
        pull: (url) => {
            const net = new NetProvider();
            net.get(url, (res) => {
                let dataKey = `${url}`
                const state = {
                }
                state[dataKey] = res['data']
                dispatch({
                    type: 'update',
                    state: state
                });
            }, (error) => {
                /**
                notification.open({
                    message: '错误',
                    description: '你无法访问，可能是页面不存在.',
                });
                */
                console.log('发生错误', error)
                let dataKey = `${url}`
                const state = {
                }
                state[dataKey] = ''
                dispatch({
                    type: 'update',
                    state: state
                })
            });
        }
    }
}

export default connect(mapState2PropsMarkDown, mapDispatch2PropsMarkDown)(Fragment)