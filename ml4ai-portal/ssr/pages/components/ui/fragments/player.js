import React from 'react'

import { connect } from 'react-redux'

const Component = React.Component;

class Player extends Component {

    constructor(props) {
        super(props);
        this.player = React.createRef()
    }

    componentWillMount() {

    }

    componentDidMount() {
        if (flvjs) {
            if (flvjs.isSupported()) {
                let flvPlayer = flvjs.createPlayer({
                    type: 'flv',
                    url: 'http://upos-hz-mirrorkodou.acgvideo.com/upgcxcode/99/73/74077399/74077399-1-32.flv'
                });
                let e = document.getElementById("player")
                flvPlayer.attachMediaElement(e)
                flvPlayer.load()
                flvPlayer.play()
            }
        } else {
            alert("你的浏览器不支持播放器")
        }
    }

    componentWillReceiveProps(props) {

    }

    render() {
        return (<div>
            <div className="width100height100" id="player"></div>
        </div>);
    }

}

let mapState2PropsMarkDown = (state) => {
    return { ...state }
}

let mapDispatch2PropsMarkDown = (dispatch) => {
    return {
    }
}

export default connect(mapState2PropsMarkDown, mapDispatch2PropsMarkDown)(Player)