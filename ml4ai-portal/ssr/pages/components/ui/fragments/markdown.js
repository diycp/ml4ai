import React from 'react'
import ReactMarkdown from 'react-markdown'
import NetProvider from '../../net/net'

import { connect } from 'react-redux'

const Component = React.Component;

class Markdown extends Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.pull(this.props.url)
    }

    componentDidMount() {
        this.props.pull(this.props.url)
    }

    componentWillReceiveProps(props) {
        props.pull(props.url)
    }

    render() {
        return (<div>
            <ReactMarkdown escapeHtml={false} skipHtml={false} unwrapDisallowed={false} source={this.props[this.props.url]}></ReactMarkdown>
        </div>);
    }

}

let mapState2PropsMarkDown = (state) => {
    return { ...state }
}

let mapDispatch2PropsMarkDown = (dispatch) => {
    return {
        pull: (url) => {
            const net = new NetProvider();
            net.get(url, (res) => {
                let dataKey = `${url}`
                const state = {
                }
                state[dataKey] = res['data']
                dispatch({
                    type: 'update',
                    state: state
                });
            }, (error) => {
                let dataKey = `${url}`
                const state = {
                }
                state[dataKey] = ''
                dispatch({
                    type: 'update',
                    state: state
                })
            });
        }
    }
}

export default connect(mapState2PropsMarkDown, mapDispatch2PropsMarkDown)(Markdown)