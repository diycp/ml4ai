import React from 'react'
import RootPage from './components/ui/pages/root.page';

export default class Index extends React.Component {

    constructor(prop) {
        super(prop);
    }

    render() {
        return <div className={"full"}>
            <RootPage />
        </div>;
    }
}