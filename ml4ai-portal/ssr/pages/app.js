import React from 'react'
import Index from '../pages/index'
import { Route } from 'react-router'
import './css/app.css'

export default class App extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Route path="/" component={Index}></Route>
            </div>)
    }

}