const path = require('path')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const copyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
    target: 'node',
    entry: {
        app: ['babel-polyfill', path.join(__dirname, './server/server-entry.js')],
    },
    output: {
        filename: 'server-entry.js',
        path: path.join(__dirname, './dist'),
        publicPath: "",
        libraryTarget: 'commonjs2'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                use: [{
                    loader: "babel-loader",
                    options: {
                        "presets": [
                            "env",
                            "es2015",
                            "react",
                            "stage-0",
                            "stage-2"
                        ],
                        plugins: [
                            'transform-decorators-legacy',
                            'transform-runtime',
                            'babel-plugin-transform-runtime'
                        ]
                    }
                }],
                exclude: [
                    path.join(__dirname, "../node_modules")
                ],
            }, {
                test: /\.(s)?css$/,
                exclude: /node_modules/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.(png|jpg|gif|woff|svg|eot|woff2|tff)$/,
                use: 'url-loader?limit=8129',
                //注意后面那个limit的参数，当你图片大小小于这个限制的时候，会自动启用base64编码图片
                exclude: /node_modules/
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(path.join(__dirname, '/dist/*.*'), {
            root: __dirname,
            verbose: true,
            dry: false
        }),
        new copyWebpackPlugin([{
            from: path.resolve(__dirname, 'assets'),
            to: path.resolve(__dirname, 'dist', 'assets')
        }]),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css'
        })
    ]
}