import React from 'react';
import ReactDOM from 'react-dom';
import App from '../pages/app';
import { BrowserRouter } from 'react-router-dom';
import { createStore } from 'redux'
import { Provider } from 'react-redux'

const reducer = (state = {}, action) => {
    const target = Object.assign({}, state, action.state)
    switch (action.type) {
        default:
            return target
    }

}

const store = createStore(reducer, window && window.INIT_STATE ? window.INIT_STATE : {})

const unsub = store.subscribe(() => {
    let state = store.getState()
})

ReactDOM.hydrate(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);