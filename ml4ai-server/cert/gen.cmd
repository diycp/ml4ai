keytool -genkeypair -alias "ml4ai" -keyalg "RSA" -storepass ml4ai-backend -keystore "ml4ai.keystore" -keypass ml4ai-backend -keysize 2048
keytool -list -keystore ml4ai.keystore
keytool -export -alias ml4ai -file ml4ai.crt -keystore ml4ai.keystore