package com.ml4ai.backend.security;

import com.google.gson.JsonObject;
import com.ml4ai.backend.utils.JSONUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by leecheng on 2017/10/18.
 */
public class SecurityRequestFilter implements Filter {

    @Value("${security.authURL}")
    private String authInformationSource;

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String contentType = request.getHeader("Content-Type");
        if (StringUtils.isEmpty(contentType)) {
            chain.doFilter(servletRequest, servletResponse);
        } else if (contentType.trim().toUpperCase().contains("APPLICATION/JSON") && request.getRequestURI().replaceAll("/+", "/").equals(authInformationSource)) {
            if (!StringUtils.isEmpty(request.getHeader("Content-Length"))) {
                InputStream inputStream = request.getInputStream();
                ByteArrayOutputStream bo = new ByteArrayOutputStream();
                int cnt;
                byte[] buff = new byte[1024];
                while ((cnt = inputStream.read(buff)) != -1) {
                    bo.write(buff, 0, cnt);
                    bo.flush();
                }
                byte[] bodyData = bo.toByteArray();
                String body = new String(bodyData, "UTF-8");
                JsonObject jsonObject = JSONUtil.toJsonObject(body);
                HttpServletRequest proxyHttpServletRequest = (HttpServletRequest) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{HttpServletRequest.class}, new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        if (method.getName().equals("getInputStream")) {
                            return new DelegateServletInputStream(new ByteArrayInputStream(bodyData), request.getContentLength());
                        } else if (method.getName().equals("getParameter") && method.getParameterCount() == 1 && method.getParameterTypes()[0] == String.class) {
                            return jsonObject.get((String) args[0]).toString();
                        }
                        return method.invoke(request, args);
                    }
                });
                chain.doFilter(proxyHttpServletRequest, response);
            } else {
                chain.doFilter(request, response);
            }
        } else {
            chain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
}
