package com.ml4ai.backend.security;

import com.ml4ai.backend.dto.UserAgent;
import com.ml4ai.backend.dto.UserDTO;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by uesr on 2018/9/12.
 */
public class SecurityUtils {

    public static UserDTO getCurrentUser() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        if (authentication != null) {
            Object principal = authentication.getPrincipal();
            if (principal != null && principal instanceof UserAgent) {
                UserAgent user = (UserAgent) principal;
                return user.getUser();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

}
