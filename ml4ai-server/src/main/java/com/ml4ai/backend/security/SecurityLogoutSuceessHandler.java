package com.ml4ai.backend.security;

import com.ml4ai.backend.utils.JSONUtil;
import com.ml4ai.backend.utils.RestUtil;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by leecheng on 2017/10/17.
 */
public class SecurityLogoutSuceessHandler implements LogoutSuccessHandler {

    public static final String NAME = "hbz_logout_success_handler";

    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        httpServletRequest.setCharacterEncoding("UTF-8");
        httpServletResponse.setCharacterEncoding("UTF-8");
        httpServletResponse.addHeader("Access-Control-Allow-Origin", "*");
        httpServletResponse.setHeader("Content-Type", "application/json;charset=utf-8");
        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        httpServletResponse.getWriter().write(JSONUtil.toJson(RestUtil.unAuthorized()));
    }
}
