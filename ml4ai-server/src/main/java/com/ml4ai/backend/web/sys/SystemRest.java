package com.ml4ai.backend.web.sys;

import com.ml4ai.backend.dto.TicketDTO;
import com.ml4ai.backend.dto.UserDTO;
import com.ml4ai.backend.security.SecurityUtils;
import com.ml4ai.backend.services.TicketService;
import com.ml4ai.backend.utils.RestUtil;
import com.ml4ai.backend.utils.StringHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by uesr on 2018/9/16.
 */
@Api("用户接口")
@RestController
@RequestMapping("/api/sys/user")
public class SystemRest {

    @Autowired
    TicketService ticketService;

    @ApiOperation("当前用户票据获取")
    @RequestMapping(value = "/ticket", method = {RequestMethod.POST, RequestMethod.GET})
    public Map<String, Object> ticket() {
        UserDTO user = SecurityUtils.getCurrentUser();
        if (user == null) {
            return RestUtil.failure();
        } else {
            TicketDTO ticket = new TicketDTO();
            ticket.setStatus("1");
            ticket.setTicket(StringHelper.uuid());
            ticket.setUser(user);
            ticket = ticketService.save(ticket);
            return RestUtil.success(ticket.getTicket());
        }
    }

}
