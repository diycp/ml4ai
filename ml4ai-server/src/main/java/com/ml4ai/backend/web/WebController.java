package com.ml4ai.backend.web;

import com.ml4ai.backend.utils.WsHost;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

/**
 * Created by uesr on 2018/9/16.
 */
@Component
@Slf4j
@ServerEndpoint(value = "/api/stream/socket")
public class WebController {

    @OnOpen
    public void open(Session session) {
        WsHost.create(session);
    }

    @OnClose
    public void onClose() {
        log.debug("连接关闭");
    }

    @OnMessage
    public void onMessage(String message, Session session) throws IOException {

    }

    @OnError
    public void onError(Session session, Throwable error) {
        error.printStackTrace();
    }


}
