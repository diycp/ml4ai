package com.ml4ai.backend.web;

import com.ml4ai.backend.dto.MapAddressDTO;
import com.ml4ai.backend.dto.MapRouteMatrixDTO;
import com.ml4ai.backend.services.MapService;
import com.ml4ai.backend.utils.RestUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by leecheng on 2017/11/29.
 */
@Api(value = "地图接口")
@RestController
@RequestMapping("/api/map")
public class MapMartrixRest {

    @Autowired
    private MapService service;

    /**
     * @param addr
     * @return
     */
    @ApiOperation(value = "地址转坐标")
    @RequestMapping(value = "/geo/convert", method = RequestMethod.POST)
    public Map<String, Object> queryPointByAdd(@RequestBody MapAddressDTO addressDTO) {
        if (StringUtils.isNotEmpty(addressDTO.getAdd())) {
            Map<String, Object> add = service.getLocation(addressDTO.getAdd());
            if (add != null) {
                return RestUtil.success(add);
            } else {
                return RestUtil.failure();
            }
        } else {
            return RestUtil.failure("地址为空");
        }
    }

    @ApiOperation("坐标转信息")
    @RequestMapping(value = "/geo/info", method = RequestMethod.POST)
    public Map<String, Object> queryAddByPoint(@RequestBody MapAddressDTO addressDTO) {
        if (addressDTO.getLng() != null && addressDTO.getLat() != null) {
            Map<String, Object> add = service.getLocation(addressDTO.getLng(), addressDTO.getLat());
            if (add != null) {
                return RestUtil.success(add);
            } else {
                return RestUtil.failure();
            }
        } else {
            return RestUtil.failure("坐标为空");
        }
    }

    @ApiOperation("路线计算")
    @RequestMapping(value = {"/routeMatrix"}, method = RequestMethod.POST)
    public Map<String, Object> queryRoute(@RequestBody MapRouteMatrixDTO matrix) {
        Map<String, Object> ret = service.route(matrix);
        if (ret != null) {
            return RestUtil.success(ret);
        } else {
            return RestUtil.failure("错误");
        }
    }

}
