package com.ml4ai.backend.web.sys;

import com.ml4ai.backend.dto.MenuDTO;
import com.ml4ai.backend.services.MenuService;
import com.ml4ai.backend.utils.RestUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by uesr on 2018/9/4.
 */
@Api("菜单管理")
@CrossOrigin
@RestController
@RequestMapping("/api/menu")
public class MenuRest {

    @Autowired
    MenuService menuService;

    @ApiOperation(value = "得到菜单树")
    @RequestMapping(value = "/get", method = {RequestMethod.POST, RequestMethod.GET})
    public Map<String, Object> get() {
        return RestUtil.success(menuService.getAllMenus());
    }

    @ApiOperation(value = "查询菜单", httpMethod = "POST")
    @RequestMapping(value = "/query", method = {RequestMethod.POST})
    public Map<String, Object> query(@RequestBody MenuDTO menuDTO) {
        menuDTO.setStatus("1");
        List<MenuDTO> sub = menuService.query(menuDTO);
        return RestUtil.success(sub);
    }

    @ApiOperation(value = "保存菜单", httpMethod = "POST")
    @RequestMapping(value = "/save", method = {RequestMethod.POST})
    public Map<String, Object> save(@RequestBody MenuDTO menu) {
        menu.setStatus("1");
        MenuDTO save = menuService.save(menu);
        return RestUtil.success(save);
    }

    @ApiOperation(value = "删除菜单", httpMethod = "POST")
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    public Map<String, Object> delete(@RequestBody MenuDTO menu) {
        return RestUtil.success(menuService.delete(menu.getId()));
    }
}
