package com.ml4ai.backend.utils;

import lombok.Cleanup;
import lombok.SneakyThrows;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Created by uesr on 2018/9/2.
 */
public class ExceptionUtils {

    @SneakyThrows
    public static String getStack(Throwable t) {
        @Cleanup ByteArrayOutputStream os = new ByteArrayOutputStream();
        @Cleanup PrintStream ps = new PrintStream(os);
        t.printStackTrace(ps);
        return new String(os.toByteArray());
    }

}
