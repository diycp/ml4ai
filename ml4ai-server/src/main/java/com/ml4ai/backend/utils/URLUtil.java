package com.ml4ai.backend.utils;

import javax.validation.constraints.NotNull;

/**
 * Created by leecheng on 2018/10/21.
 */
public class URLUtil {

    public static String urlToMatch(@NotNull String url) {
        String regex = "";
        for (int i = 0; i < url.length(); i++) {
            String c = url.substring(i, i + 1);
            if (c.toLowerCase().equals(c.toUpperCase())) {
                switch (c) {
                    case ".":
                    case "?":
                    case "-":
                    case "[":
                    case "]":
                    case "{":
                    case "}":
                    case "*":
                    case "+":
                    case "\\":
                    case "(":
                    case ")":
                    case "=":
                    case "^":
                    case "$":
                    case "%":
                        regex += "\\" + c;
                        break;
                    case " ":
                        regex += "\\s";
                        break;
                    default:
                        regex += c;
                        break;
                }
            } else {
                regex += "[" + c.toUpperCase() + c.toLowerCase() + "]";
            }
        }
        return regex;
    }

}
