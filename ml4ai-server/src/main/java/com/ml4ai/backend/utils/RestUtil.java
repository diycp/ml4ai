package com.ml4ai.backend.utils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by 李程 on 2018/9/2.
 */
public class RestUtil {

    public static Map<String, Object> success(Object data) {
        Map<String, Object> res = new LinkedHashMap<>();
        res.put("code", "200");
        res.put("msg", "操作ok了");
        res.put("data", data);
        return res;
    }

    public static Map<String, Object> failure(String msg, Object data) {
        Map<String, Object> result = new LinkedHashMap<>();
        result.put("code", "500");
        result.put("msg", msg);
        result.put("data", data);
        return result;
    }

    public static Map<String, Object> failure(Object data) {
        return failure("操作有误", data);
    }

    public static Map<String, Object> failure(String msg) {
        return failure(msg, null);
    }

    public static Map<String, Object> failure() {
        return failure(null);
    }

    public static Map<String, Object> success() {
        return success(null);
    }

    public static Map<String, Object> unAuthorized(Object data) {
        Map<String, Object> res = new LinkedHashMap<>();
        res.put("code", "401");
        res.put("msg", "需要认证");
        res.put("data", data);
        return res;
    }

    public static Map<String, Object> unAuthorized() {
        return unAuthorized(null);
    }

    public static Map<String, Object> forbidden(Object data) {
        Map<String, Object> response = new LinkedHashMap<>();
        response.put("code", "403");
        response.put("msg", "禁止访问");
        response.put("data", data);
        return response;
    }

    public static Map<String, Object> forbidden() {
        return forbidden(null);
    }
}
