package com.ml4ai.backend.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class SpringUtils implements ApplicationContextAware, EnvironmentAware {

    private static ApplicationContext applicationContext;

    private static Environment environment;

    @Override
    public void setApplicationContext(ApplicationContext app) throws BeansException {
        applicationContext = app;
    }

    @Override
    public void setEnvironment(Environment en) {
        environment = en;
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static Environment getEnvironment() {
        return environment;
    }

    public static <T> T getService(Class<T> clazz) {
        return (T) applicationContext.getBean(clazz);
    }

    public static <T> Map<String, T> getBeans(Class<T> tClass) {
        return applicationContext.getBeansOfType(tClass);
    }
}
