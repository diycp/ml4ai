package com.ml4ai.backend.utils;

import java.io.Closeable;

public class CloseUtils {

    public static void close(Closeable closeable) {
        try {
            closeable.close();
        } catch (Exception e) {

        }
    }
}
