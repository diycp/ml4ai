package com.ml4ai.backend.stack.webcollector;

import cn.edu.hfut.dmic.webcollector.crawldb.DBManager;
import cn.edu.hfut.dmic.webcollector.crawler.AutoParseCrawler;

/**
 * Created by leecheng on 2018/10/19.
 */
public abstract class Ml4aiAutoParseCrawler extends AutoParseCrawler {

    public Ml4aiAutoParseCrawler(DBManager dbManager, boolean autoParse) {
        super(autoParse);
        this.dbManager = dbManager;
    }


}
