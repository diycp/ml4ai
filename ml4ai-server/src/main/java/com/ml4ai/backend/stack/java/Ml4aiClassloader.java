package com.ml4ai.backend.stack.java;

/**
 * Created by leecheng on 2018/10/27.
 */
public class Ml4aiClassloader extends ClassLoader {

    private SourceLoader sourceLoader;

    public Ml4aiClassloader(SourceLoader sourceLoader, ClassLoader parent) {
        super(parent);
        this.sourceLoader = sourceLoader;
    }

    @Override
    public Class<?> findClass(String name) throws ClassNotFoundException {
        byte[] classData = sourceLoader.getSource(name);
        return defineClass(name, classData, 0, classData.length);
    }

    @FunctionalInterface
    public interface SourceLoader {
        byte[] getSource(String name);
    }
}
