package com.ml4ai.backend.stack.redis.io;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by leecheng on 2018/10/21.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProxySerializer {

    private String value;

}
