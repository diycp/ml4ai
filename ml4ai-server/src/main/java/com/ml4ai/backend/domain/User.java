package com.ml4ai.backend.domain;

import com.ml4ai.backend.domain.base.BaseAuditEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Created by uesr on 2018/9/8.
 */
@Getter
@Setter
@Entity
@Table(name = "T_USER")
public class User extends BaseAuditEntity {

    @Column(name = "c_login")
    private String login;

    @Column(name = "c_password")
    private String password;

    @Column(name = "c_telephone")
    private String telephone;

    @Column(name = "c_nick")
    private String nick;

    @Column(name = "c_mail")
    private String mail;

    @ManyToMany
    @JoinTable(name = "T_USER_ROLE", joinColumns = {@JoinColumn(name = "c_user")}, inverseJoinColumns = {@JoinColumn(name = "c_role")})
    private List<Role> roles;
}
