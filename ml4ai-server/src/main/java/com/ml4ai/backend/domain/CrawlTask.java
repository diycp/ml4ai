package com.ml4ai.backend.domain;

import com.ml4ai.backend.domain.base.BaseAuditEntity;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by leecheng on 2018/10/27.
 */
@Entity
@Table(name = "T_CRAWL")
@Getter
@Setter
public class CrawlTask extends BaseAuditEntity {

    @Column
    private String taskName;

    @Cascade(CascadeType.ALL)
    @ManyToMany
    @JoinTable(name = "T_CRAWL_SEED", joinColumns = @JoinColumn(name = "CRAWL"), inverseJoinColumns = @JoinColumn(name = "URL"))
    private List<Text> seeds;

    @Cascade(CascadeType.ALL)
    @ManyToMany
    @JoinTable(name = "T_CRAWL_REGEX", joinColumns = @JoinColumn(name = "CRAWL"), inverseJoinColumns = @JoinColumn(name = "URL"))
    private List<Text> regex;

    @Column
    private Long startTime;

}
