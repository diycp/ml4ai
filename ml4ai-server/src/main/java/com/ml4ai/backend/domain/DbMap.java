package com.ml4ai.backend.domain;

import com.ml4ai.backend.domain.base.BaseAuditEntity;
import lombok.*;

import javax.persistence.*;

/**
 * Created by sleec on 2018/10/27.
 */
@Table(name = "T_MAPPING")
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DbMap extends BaseAuditEntity {

    @Column(name = "c_name")
    private String name;

    @Column(name = "c_key_")
    private String key;

    @Lob
    @Column(name = "c_value")
    private String value;

}
