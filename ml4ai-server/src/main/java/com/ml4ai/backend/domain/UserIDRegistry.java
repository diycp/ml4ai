package com.ml4ai.backend.domain;

import com.ml4ai.backend.domain.base.RegistryStatus;
import com.ml4ai.backend.domain.base.Sex;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by uesr on 2018/9/8.
 */
@Setter
@Getter
@Entity
@Table(name = "T_USER_ID_REGISTRY")
@PrimaryKeyJoinColumn
public class UserIDRegistry extends UserRegistry {

    @Column(name = "c_id_no")
    private String idNo;

    @Enumerated(EnumType.STRING)
    @Column(name = "c_sex")
    private Sex sex;

    @Column(name = "c_id_card_reference")
    private String idCardReference;

    @Column(name = "c_hand_id_card_reference")
    private String handIDCardReference;

    @Enumerated(EnumType.STRING)
    @Column(name = "c_registry_status")
    private RegistryStatus registryStatus;

    @Column(name = "c_authTime")
    private Long authTime;

}
