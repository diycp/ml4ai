package com.ml4ai.backend.domain.base;

import lombok.Getter;

/**
 * Created by uesr on 2018/9/8.
 */
@Getter
public enum Sex {

    Male("男"),
    Female("女"),
    Company("公司");

    private String name;

    private Sex(String name) {
        this.name = name;
    }

}
