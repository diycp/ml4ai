package com.ml4ai.backend.domain;

import com.ml4ai.backend.domain.base.BaseAuditEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by leecheng on 2018/11/24.
 */
@Getter
@Setter
@Entity
@Table(name = "T_TEXT_MEDIA")
public class TextMedia extends BaseAuditEntity {

    private String title;

    private String introduce;

    private String detail;

}
