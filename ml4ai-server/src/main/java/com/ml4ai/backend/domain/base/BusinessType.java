package com.ml4ai.backend.domain.base;

import lombok.Getter;

/**
 * Created by uesr on 2018/9/9.
 */
@Getter
public enum BusinessType {
    Buy("帮买"),
    IntraCityDistribution("同城配送");

    private String name;

    private BusinessType(String name) {
        this.name = name;
    }
}
