package com.ml4ai.backend.domain;

import com.ml4ai.backend.domain.base.BaseAuditEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by uesr on 2018/9/2.
 */

@Entity
@Table(name = "T_MENU")
@Getter
@Setter
public class Menu extends BaseAuditEntity {

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "c_parent_id")
    private Menu parent;

    @Column(name = "c_menu_name")
    private String menuName;

    @Lob
    @Column(name = "c_menu_data")
    private String menuData;

    @Column(name = "c_details")
    private String details;

    @Column(name = "c_sort_no")
    private Integer sortNo;

    public Menu getParent() {
        return parent;
    }

    public void setParent(Menu parent) {
        this.parent = parent;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuData() {
        return menuData;
    }

    public void setMenuData(String menuData) {
        this.menuData = menuData;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }
}
