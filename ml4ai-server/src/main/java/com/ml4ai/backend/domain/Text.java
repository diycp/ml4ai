package com.ml4ai.backend.domain;

import com.ml4ai.backend.domain.base.BaseAuditEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by leecheng on 2018/10/27.
 */
@Entity
@Table(name = "T_TEXT")
@Getter
@Setter
public class Text extends BaseAuditEntity {

    @Column(name = "c_value")
    private String value;

}
