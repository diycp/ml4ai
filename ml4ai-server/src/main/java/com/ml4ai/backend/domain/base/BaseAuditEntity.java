package com.ml4ai.backend.domain.base;

import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

/**
 * Created by 李程 on 2018/9/2.
 */
@Getter
@Setter
@EntityListeners(value = AuditingEntityListener.class)
@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
public class BaseAuditEntity extends BaseEntity {

    @CreatedDate
    @Column(name = "data_create_time")
    private Long createTime;

    @LastModifiedDate
    @Column(name = "data_last_update_time")
    private Long lastUpdateTime;

    @CreatedBy
    @Column(name = "data_create_by")
    private Long createBy;

    @LastModifiedBy
    @Column(name = "data_last_update_by")
    private Long lastUpdateBy;

    @Column(name = "data_status")
    private String status;
}
