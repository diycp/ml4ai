package com.ml4ai.backend.domain;

import com.ml4ai.backend.domain.base.BaseAuditEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by uesr on 2018/9/10.
 */
@Getter
@Setter
@Entity
@Table(name = "T_SITE")
public class Site extends BaseAuditEntity {

    @ManyToOne
    @JoinColumn(name = "c_parent")
    private Site parent;

    @Column(name = "c_name")
    private String name;

}
