package com.ml4ai.backend.services.mappers;

import com.ml4ai.backend.consts.Const;
import com.ml4ai.backend.domain.IntraCityDistributionTask;
import com.ml4ai.backend.dto.IntraCityDistributionTaskDTO;
import com.ml4ai.backend.repository.IntraCityDistributionTaskRepo;
import com.ml4ai.backend.repository.SiteRepository;
import com.ml4ai.backend.repository.UserRepo;
import com.ml4ai.backend.utils.Bean2Bean;
import com.ml4ai.backend.utils.PropertyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by uesr on 2018/9/12.
 */
@Component
public class IntraCityDistributionTaskMapper extends BaseMapper<IntraCityDistributionTask, IntraCityDistributionTaskDTO> {

    @Autowired
    IntraCityDistributionTaskRepo intraCityDistributionTaskRepo;
    @Autowired
    UserMapper userMapper;
    @Autowired
    SiteMapper siteMapper;
    @Autowired
    UserRepo userRepo;
    @Autowired
    SiteRepository siteRepo;

    @Override
    public IntraCityDistributionTask newEntity() {
        return new IntraCityDistributionTask();
    }

    @Override
    public IntraCityDistributionTaskDTO newDTO() {
        return new IntraCityDistributionTaskDTO();
    }

    @Override
    public IntraCityDistributionTask find(Long id) {
        return intraCityDistributionTaskRepo.getOne(id);
    }

    @Override
    public void entity2dto(IntraCityDistributionTask entity, IntraCityDistributionTaskDTO dto) {
        new Bean2Bean().addPropMapper(
                new PropertyMapper<>("starter", userMapper::e2d),
                new PropertyMapper<>("starter.id", "starterId"),
                new PropertyMapper<>("taker", userMapper::e2d),
                new PropertyMapper<>("taker.id", "takerId"),
                new PropertyMapper<>("site", siteMapper::e2d),
                new PropertyMapper<>("site.id", "siteId")
        ).copyProperties(entity, dto);
    }

    @Override
    public void dto2entity(IntraCityDistributionTaskDTO dto, IntraCityDistributionTask entity) {
        new Bean2Bean().addExcludeProp("starter", "taker", "site").addExcludeProp(Const.getNotCopyAuditFields()).copyProperties(dto, entity);

        Long siteId = Bean2Bean.getFirstNoNullVal(dto, "siteId", "site.id");
        if (siteId != null) {
            entity.setSite(siteRepo.getOne(siteId));
        } else {
            entity.setSite(null);
        }
        Long starterId = Bean2Bean.getFirstNoNullVal(dto, "starterId", "starter.id");
        if (starterId != null) {
            entity.setStarter(userRepo.getOne(starterId));
        } else {
            entity.setStarter(null);
        }
        Long takerId = Bean2Bean.getFirstNoNullVal(dto, "takerId", "taker.id");
        if (takerId != null) {
            entity.setTaker(userRepo.getOne(takerId));
        } else {
            entity.setTaker(null);
        }
    }
}
