package com.ml4ai.backend.services.impl;

import com.ml4ai.backend.domain.CrawlTask;
import com.ml4ai.backend.dto.CrawlTaskDTO;
import com.ml4ai.backend.repository.CrawlTaskRepo;
import com.ml4ai.backend.services.CrawlTaskService;
import com.ml4ai.backend.services.base.impl.BaseServiceImpl;
import com.ml4ai.backend.services.mappers.CrawlTaskMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.function.Function;

/**
 * Created by leecheng on 2018/10/28.
 */
@Service
@Transactional
public class CrawlTaskServiceImpl extends BaseServiceImpl<CrawlTask, CrawlTaskDTO> implements CrawlTaskService {

    @Autowired
    CrawlTaskRepo crawlTaskRepo;

    @Autowired
    CrawlTaskMapper crawlTaskMapper;

    @Override
    public JpaRepository<CrawlTask, Long> getRepository() {
        return crawlTaskRepo;
    }

    @Override
    public Function<CrawlTask, CrawlTaskDTO> getConvertEntity2DTOFunction() {
        return crawlTaskMapper::e2d;
    }

    @Override
    public Function<CrawlTaskDTO, CrawlTask> getConvertDTO2EntityFunction() {
        return crawlTaskMapper::d2e;
    }
}
