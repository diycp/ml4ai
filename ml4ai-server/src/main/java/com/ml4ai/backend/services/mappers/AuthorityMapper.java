package com.ml4ai.backend.services.mappers;

import com.ml4ai.backend.consts.Const;
import com.ml4ai.backend.domain.Authority;
import com.ml4ai.backend.dto.AuthorityDTO;
import com.ml4ai.backend.repository.AuthorityRepo;
import com.ml4ai.backend.utils.Bean2Bean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by leecheng on 2018/9/24.
 */
@Component
public class AuthorityMapper extends BaseMapper<Authority, AuthorityDTO> {

    @Autowired
    AuthorityRepo authorityRepo;

    @Override
    public Authority newEntity() {
        return new Authority();
    }

    @Override
    public AuthorityDTO newDTO() {
        return new AuthorityDTO();
    }

    @Override
    public Authority find(Long id) {
        return authorityRepo.getOne(id);
    }

    @Override
    public void entity2dto(Authority entity, AuthorityDTO dto) {
        new Bean2Bean().copyProperties(entity, dto);
    }

    @Override
    public void dto2entity(AuthorityDTO dto, Authority entity) {
        new Bean2Bean().addExcludeProp(Const.getNotCopyAuditFields()).copyProperties(dto, entity);
    }
}
