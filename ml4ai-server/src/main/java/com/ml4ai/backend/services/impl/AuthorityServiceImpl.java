package com.ml4ai.backend.services.impl;

import com.ml4ai.backend.domain.Authority;
import com.ml4ai.backend.dto.AuthorityDTO;
import com.ml4ai.backend.repository.AuthorityRepo;
import com.ml4ai.backend.services.AuthorityService;
import com.ml4ai.backend.services.base.impl.BaseServiceImpl;
import com.ml4ai.backend.services.mappers.AuthorityMapper;
import com.ml4ai.backend.utils.SpringUtils;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.function.Function;

/**
 * Created by leecheng on 2018/9/24.
 */
@Service
@Transactional
public class AuthorityServiceImpl extends BaseServiceImpl<Authority, AuthorityDTO> implements AuthorityService {

    @Override
    public Function<Authority, AuthorityDTO> getConvertEntity2DTOFunction() {
        return SpringUtils.getService(AuthorityMapper.class)::e2d;
    }

    @Override
    public Function<AuthorityDTO, Authority> getConvertDTO2EntityFunction() {
        return SpringUtils.getService(AuthorityMapper.class)::d2e;
    }

    @Override
    public JpaRepository<Authority, Long> getRepository() {
        return SpringUtils.getService(AuthorityRepo.class);
    }
}
