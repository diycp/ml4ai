package com.ml4ai.backend.services;

import com.ml4ai.backend.domain.Menu;
import com.ml4ai.backend.dto.MenuDTO;
import com.ml4ai.backend.services.base.BaseService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by uesr on 2018/9/2.
 */
@Service
public interface MenuService extends BaseService<Menu, MenuDTO> {

    List<MenuDTO> getAllMenus();

}
