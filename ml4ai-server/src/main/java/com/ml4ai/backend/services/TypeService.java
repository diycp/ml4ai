package com.ml4ai.backend.services;

import com.ml4ai.backend.domain.Type;
import com.ml4ai.backend.dto.TypeDTO;
import com.ml4ai.backend.services.base.BaseService;

/**
 * Created by uesr on 2018/9/12.
 */
public interface TypeService extends BaseService<Type, TypeDTO> {
}
