package com.ml4ai.backend.services;

import com.ml4ai.backend.domain.CrawlTask;
import com.ml4ai.backend.dto.CrawlTaskDTO;
import com.ml4ai.backend.services.base.BaseService;

/**
 * Created by leecheng on 2018/10/28.
 */
public interface CrawlTaskService extends BaseService<CrawlTask, CrawlTaskDTO> {
}
