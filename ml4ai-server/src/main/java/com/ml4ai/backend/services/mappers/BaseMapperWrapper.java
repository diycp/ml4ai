package com.ml4ai.backend.services.mappers;

import com.ml4ai.backend.domain.base.BaseAuditEntity;
import com.ml4ai.backend.dto.base.BaseAuditDTO;

/**
 * Created by leecheng on 2018/7/6.
 */
public interface BaseMapperWrapper<E extends BaseAuditEntity, D extends BaseAuditDTO> {

    E newEntity();

    D newDTO();

    E find(Long id);

    D e2d(E entity);

    E d2e(D dto);

}
