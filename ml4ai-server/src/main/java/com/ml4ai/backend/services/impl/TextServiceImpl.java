package com.ml4ai.backend.services.impl;

import com.ml4ai.backend.domain.Text;
import com.ml4ai.backend.dto.TextDTO;
import com.ml4ai.backend.repository.TextRepo;
import com.ml4ai.backend.services.TextService;
import com.ml4ai.backend.services.base.impl.BaseServiceImpl;
import com.ml4ai.backend.services.mappers.TextMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.function.Function;

/**
 * Created by leecheng on 2018/10/28.
 */
@Service
@Transactional
public class TextServiceImpl extends BaseServiceImpl<Text, TextDTO> implements TextService {

    @Autowired
    TextRepo textRepo;

    @Autowired
    TextMapper textMapper;

    @Override
    public Function<Text, TextDTO> getConvertEntity2DTOFunction() {
        return textMapper::e2d;
    }

    @Override
    public Function<TextDTO, Text> getConvertDTO2EntityFunction() {
        return textMapper::d2e;
    }

    @Override
    public JpaRepository<Text, Long> getRepository() {
        return textRepo;
    }
}
