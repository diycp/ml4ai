package com.ml4ai.backend.services;

import com.ml4ai.backend.domain.Text;
import com.ml4ai.backend.dto.TextDTO;
import com.ml4ai.backend.services.base.BaseService;

/**
 * Created by leecheng on 2018/10/28.
 */
public interface TextService extends BaseService<Text, TextDTO> {
}
