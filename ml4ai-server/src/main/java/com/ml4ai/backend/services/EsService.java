package com.ml4ai.backend.services;

import java.util.List;
import java.util.Map;

/**
 * Created by leecheng on 2018/10/13.
 */
public interface EsService {

    boolean putJson(String index, String type, String id, Map<String, Object> document);

    boolean deleteDocument(String index, String type, String id);

    List<Map<String, Object>> queryBool(String[] indices, String[] types, Map<String, Object> queryMap, int offset, int limit);
}
