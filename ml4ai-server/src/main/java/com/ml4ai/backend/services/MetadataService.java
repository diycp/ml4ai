package com.ml4ai.backend.services;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * Created by uesr on 2018/9/8.
 */
public interface MetadataService {

    MetadataInstance attach(String key);

    public static interface MetadataInstance {

        int put(String rowKey, Map<String, String> data);

        Map<String, String> get(String rowKey);

        List<Map<String, String>> scan(Function<Map<String, String>, Boolean> checker);

    }

}
