package com.ml4ai.backend.services;

import java.util.List;
import java.util.concurrent.Future;

/**
 * Created by leecheng on 2018/10/19.
 */
public interface WebCatchService {

    Future<Boolean> catchPage(String taskId, List<String> see, String... regex);

}
