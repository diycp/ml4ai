package com.ml4ai.backend.services;

import com.ml4ai.backend.domain.Authority;
import com.ml4ai.backend.dto.AuthorityDTO;
import com.ml4ai.backend.services.base.BaseService;

/**
 * Created by leecheng on 2018/9/24.
 */
public interface AuthorityService extends BaseService<Authority, AuthorityDTO> {
}
