package com.ml4ai.backend.services;

import com.ml4ai.backend.domain.User;
import com.ml4ai.backend.dto.UserDTO;
import com.ml4ai.backend.services.base.BaseService;

/**
 * Created by uesr on 2018/9/12.
 */
public interface UserService extends BaseService<User, UserDTO> {

    UserDTO findByLogin(String username);

}
