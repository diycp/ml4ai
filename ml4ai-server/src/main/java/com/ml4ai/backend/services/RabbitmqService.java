package com.ml4ai.backend.services;

import com.ml4ai.core.stack.mq.RabbitMQAgent;

public interface RabbitmqService {

    RabbitMQAgent getRabbitMQAgent();

}
