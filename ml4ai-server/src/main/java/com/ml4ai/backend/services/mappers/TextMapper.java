package com.ml4ai.backend.services.mappers;

import com.ml4ai.backend.consts.Const;
import com.ml4ai.backend.domain.Text;
import com.ml4ai.backend.dto.TextDTO;
import com.ml4ai.backend.repository.TextRepo;
import com.ml4ai.backend.utils.Bean2Bean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by leecheng on 2018/10/28.
 */
@Component
public class TextMapper extends BaseMapper<Text, TextDTO> {

    @Autowired
    TextRepo textRepo;

    @Override
    public Text newEntity() {
        return new Text();
    }

    @Override
    public TextDTO newDTO() {
        return new TextDTO();
    }

    @Override
    public Text find(Long id) {
        return textRepo.getOne(id);
    }

    @Override
    public void entity2dto(Text entity, TextDTO dto) {
        new Bean2Bean().copyProperties(entity, dto);
    }

    @Override
    public void dto2entity(TextDTO dto, Text entity) {
        new Bean2Bean().addExcludeProp(Const.getNotCopyAuditFields()).copyProperties(dto, entity);
    }
}
