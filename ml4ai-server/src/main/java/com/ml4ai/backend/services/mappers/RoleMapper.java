package com.ml4ai.backend.services.mappers;

import com.ml4ai.backend.consts.Const;
import com.ml4ai.backend.domain.Role;
import com.ml4ai.backend.dto.RoleDTO;
import com.ml4ai.backend.repository.RoleRepositroy;
import com.ml4ai.backend.utils.Bean2Bean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by leecheng on 2018/9/24.
 */
@Component
public class RoleMapper extends BaseMapper<Role, RoleDTO> {

    @Autowired
    RoleRepositroy roleRepositroy;

    @Override
    public Role newEntity() {
        return new Role();
    }

    @Override
    public RoleDTO newDTO() {
        return new RoleDTO();
    }

    @Override
    public Role find(Long id) {
        return roleRepositroy.getOne(id);
    }

    @Override
    public void entity2dto(Role entity, RoleDTO dto) {
        new Bean2Bean().copyProperties(entity, dto);
    }

    @Override
    public void dto2entity(RoleDTO dto, Role entity) {
        new Bean2Bean().addExcludeProp(Const.getNotCopyAuditFields()).copyProperties(dto, entity);
    }
}
