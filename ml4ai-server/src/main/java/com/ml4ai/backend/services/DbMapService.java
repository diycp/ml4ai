package com.ml4ai.backend.services;

import com.ml4ai.backend.domain.DbMap;

import java.util.Map;
import java.util.function.Function;

/**
 * Created by sleec on 2018/10/27.
 */
public interface DbMapService {

    Map<String, String> generateLocalMapWrapper(String name);

    Integer countByName(String name);

    Integer countByNameAndKey(String name, String key);

    Integer countByNameAndValue(String name, String val);

    DbMap getByNameAndKey(String name, String key);

    <T, R> R execute(Function<T, R> function, T t);

}
