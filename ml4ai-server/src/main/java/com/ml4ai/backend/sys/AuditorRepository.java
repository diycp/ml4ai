package com.ml4ai.backend.sys;

import com.ml4ai.backend.dto.UserDTO;
import com.ml4ai.backend.security.SecurityUtils;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by uesr on 2018/9/16.
 */
@Component
public class AuditorRepository implements AuditorAware<Long> {

    @Override
    public Optional<Long> getCurrentAuditor() {
        UserDTO user = SecurityUtils.getCurrentUser();
        if (user != null) {
            return Optional.of(user.getId());
        } else {
            return Optional.of(0L);
        }
    }
}
