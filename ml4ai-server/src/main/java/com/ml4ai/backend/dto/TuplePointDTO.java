package com.ml4ai.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by leecheng on 2018/9/24.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TuplePointDTO {

    private PointDTO first;
    private PointDTO second;

}
