package com.ml4ai.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Created by uesr on 2018/9/12.
 */
@Data
@AllArgsConstructor
@Builder
public class AddInfoDTO {

    private Double lng;
    private Double lat;
    private String formattedAddress;
    private String cityCode;

}
