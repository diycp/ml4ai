package com.ml4ai.backend.dto;

import com.ml4ai.backend.dto.base.BaseAuditDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by sleec on 2018/10/27.
 */
@Setter
@Getter
@AllArgsConstructor
public class DbMapDTO extends BaseAuditDTO {

    public DbMapDTO() {

    }

    private String name;

    private String key;

    private String value;

}
