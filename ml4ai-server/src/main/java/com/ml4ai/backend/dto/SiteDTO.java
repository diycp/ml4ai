package com.ml4ai.backend.dto;

import com.ml4ai.backend.domain.Site;
import com.ml4ai.backend.dto.base.BaseAuditDTO;
import lombok.Data;

/**
 * Created by uesr on 2018/9/10.
 */
@Data
public class SiteDTO extends BaseAuditDTO {

    private String name;

    private Long parentId;

    private SiteDTO parent;
}
