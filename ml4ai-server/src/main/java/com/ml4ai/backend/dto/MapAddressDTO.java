package com.ml4ai.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by leecheng on 2017/11/29.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MapAddressDTO {

    private String add;
    private Double lng;
    private Double lat;

}
