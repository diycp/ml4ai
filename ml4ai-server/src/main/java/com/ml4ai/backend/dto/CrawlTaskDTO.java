package com.ml4ai.backend.dto;

import com.ml4ai.backend.dto.base.BaseAuditDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by leecheng on 2018/10/28.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CrawlTaskDTO extends BaseAuditDTO {

    private String taskName;

    private List<TextDTO> seeds;

    private List<TextDTO> regex;

    private Long startTime;

}
