package com.ml4ai.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("com.ml4ai.backend")
@EnableJpaRepositories(basePackages = {"com.ml4ai.backend"})
@EnableJpaAuditing
public class Boot {

    static {
        System.setProperty("es.set.netty.runtime.available.processors", "false");
    }

    public static void main(String[] args) {
        SpringApplication springApp = new SpringApplication(Boot.class);
        springApp.run(args);
    }

}
