package com.ml4ai.backend.repository;

import com.ml4ai.backend.domain.Resource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by uesr on 2018/9/9.
 */
public interface ResourceRepo extends JpaRepository<Resource, Long> {

    @Query("select r from Resource r where r.status = '1'")
    List<Resource> findAvilable();

}
