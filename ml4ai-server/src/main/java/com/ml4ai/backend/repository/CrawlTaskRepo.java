package com.ml4ai.backend.repository;

import com.ml4ai.backend.domain.CrawlTask;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by leecheng on 2018/10/28.
 */
public interface CrawlTaskRepo extends JpaRepository<CrawlTask, Long> {
}
