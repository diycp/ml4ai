package com.ml4ai.backend.repository;

import com.ml4ai.backend.domain.Text;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by leecheng on 2018/10/28.
 */
public interface TextRepo extends JpaRepository<Text, Long> {
}
