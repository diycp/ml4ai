package com.ml4ai.backend.repository;

import com.ml4ai.backend.domain.Type;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by uesr on 2018/9/12.
 */
public interface TypeRepository extends JpaRepository<Type, Long> {
}
