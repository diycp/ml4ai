package com.ml4ai.backend.repository;

import com.ml4ai.backend.domain.DbMap;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by sleec on 2018/10/27.
 */
public interface DbMapRepository extends JpaRepository<DbMap, Long> {

    Integer countByNameAndStatus(String name, String status);

    Integer countByNameAndKeyAndStatus(String name, String key, String status);

    Integer countByNameAndValueAndStatus(String name, String val, String status);

    DbMap getByNameAndKeyAndStatus(String name, String key, String status);

    void deleteByNameAndKeyAndStatus(String name, String key, String status);

    void deleteByNameAndStatus(String name, String status);

    List<DbMap> getByNameAndStatus(String name, String status);

}
