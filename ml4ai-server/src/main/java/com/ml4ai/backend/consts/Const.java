package com.ml4ai.backend.consts;

/**
 * Created by uesr on 2018/9/2.
 */
public class Const {

    public static String[] getNotCopyAuditFields() {
        return new String[]{
                "createTime",
                "lastUpdateTime",
                "createBy",
                "lastUpdateBy",
        };
    }

    public final static String PUBLIC_MESSAGE = "INNER_MESSAGE";

}
