package com.ml4ai.backend.conf.redis;

import com.ml4ai.backend.stack.redis.io.UserRedisSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * Created by leecheng on 2018/10/21.
 */
@Configuration
public class RedisConfiguration {

    @Autowired
    RedisConnectionFactory redisConnectionFactory;
    @Bean
    public RedisTemplate redisTemplate() {
        RedisTemplate template = new RedisTemplate();
        template.setConnectionFactory(redisConnectionFactory);
        template.setValueSerializer(new UserRedisSerializer());
        return template;
    }


}
