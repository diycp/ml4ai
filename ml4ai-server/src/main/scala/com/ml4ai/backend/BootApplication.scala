package com.ml4ai.server.scala

import com.ml4ai.backend.Boot
import org.springframework.boot.SpringApplication

object BootApplication {

  def main(args: Array[String]) {
    val app = new SpringApplication(classOf[Boot])
    app.run(args: _*);
  }

}