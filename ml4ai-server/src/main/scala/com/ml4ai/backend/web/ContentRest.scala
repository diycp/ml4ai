package com.ml4ai.backend.web

import java.util.{Map => JavaMap}

import com.ml4ai.backend.services.EsService
import com.ml4ai.backend.utils.RestUtil
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation._

@io.swagger.annotations.Api(value = "媒体发布综合")
@RestController
@RequestMapping({
  Array("/api/content","/content"): _*
})
class ContentRest {

  @Autowired
  var esService: EsService = null

  @ApiOperation(value = "发布查询接口", notes = "此接口为媒体发布查询接口")
  @RequestMapping(value = Array("/query"), method = Array(RequestMethod.POST))
  def queryList(
                 @RequestBody map: JavaMap[String, Object],
                 @RequestParam(name = "offset") offset: Integer,
                 @RequestParam(name = "limit") limit: Integer
               ): Object = {
    RestUtil.success(esService.queryBool(Array("app-contents"), Array("default"), map, offset, limit))
  }

  @ApiOperation(value = "保存媒体发布接口", notes = "保存接口，JSON")
  @RequestMapping(value = Array("/save"), method = Array(RequestMethod.POST))
  def save(@RequestBody content: JavaMap[String, Object]): Unit = {
    val save = esService.putJson("app-contents", "default", content.get("id").asInstanceOf[String], content);
    RestUtil.success(save)
  }

  @ApiOperation(value = "删除文档", notes = "删除文档")
  @RequestMapping(value = Array("/delete"), method = Array(RequestMethod.POST))
  def del(@RequestBody content: JavaMap[String, Object]): Unit = {
    val save = esService.deleteDocument("app-contents", "default", content.get("id").asInstanceOf[String])
    RestUtil.success(save)
  }
}
