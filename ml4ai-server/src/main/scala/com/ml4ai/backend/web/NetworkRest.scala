package com.ml4ai.backend.web

import com.ml4ai.backend.services.RabbitmqService
import javax.servlet.http.HttpServletResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{PathVariable, RequestMapping, RequestMethod}

@io.swagger.annotations.Api(value = "网络模块")
@Controller
@RequestMapping({
  Array("/api/network"): _*
})
class NetworkRest {

  @Autowired
  var rabbitmqService: RabbitmqService = null

  @RequestMapping(value = Array("/source/{encodedMessage}"), method = Array(RequestMethod.GET, RequestMethod.POST))
  def pull(@PathVariable("encodedMessage") encodedMessage: String, response: HttpServletResponse): Unit = {
    val messageService = rabbitmqService.getRabbitMQAgent
    val readChannel = s"read.channel.${encodedMessage}"
    messageService.declareQueue(readChannel, true, false, false, null)
  }

}
