package com.ml4ai.backend.services.capability

import com.google.gson.{JsonObject, JsonParser}
import com.ml4ai.core.json.stringJson._

class Command(command: String) {

  val json: JsonObject = new JsonParser().parse(command).getAsJsonObject

  def getCommand(): String = json.get("command").getAsString

  def getCallback(): String = json.get("callback").getAsString

  def get(key: String): String = command.get(key)

}