package com.ml4ai.backend.services.actors.anys

import akka.actor._
import com.ml4ai.core.stack.mq.RabbitMQAgent

//控制命令
case class FileStreamTaskStart(id: String, dependency: Map[String, AnyRef])

case class FileStreamTaskStop(id: String)

//请求命令
case class FileTaskAskContinue(id: String)

case class FileTaskTellComplete(id: String)

case class FileTaskTellStop(id: String)

class FileTransfer2RabbitMQScheduleActor extends Actor {

  val actors = new java.util.LinkedHashMap[String, ActorRef]

  override def receive: Receive = {

    case FileStreamTaskStart(id, dependency) => {
      val rabbitMQAgent = dependency.get("rabbitMQAgent").get.asInstanceOf[RabbitMQAgent]
      val queue = dependency.get("queue").get.asInstanceOf[String]
      val file = dependency.get("file").get.asInstanceOf[String]
      val actor = context.actorOf(Props(new FileTransfer2RabbitMQWorkActor(id, rabbitMQAgent, queue, file)))
      actors.put(id, actor)
      actor ! "start"
    }

    case FileStreamTaskStop(id) => {
      if (actors.containsKey(id)) {
        val actor = actors.get(id)
        actor ! "stop"
        actors.remove(id)
      }
    }

    case FileTaskAskContinue(id) => {
      if (actors.containsKey(id)) {
        sender ! "step"
      }
    }

    case FileTaskTellComplete(id) => {
      actors.remove(id)
    }

    case FileTaskTellStop(id) => {
      actors.remove(id)
    }

    case message: AnyRef => {
      println(s"无法识别的指令：${message}，支持：${FileStreamTaskStart},${FileStreamTaskStop} 命令")
    }
  }

}
