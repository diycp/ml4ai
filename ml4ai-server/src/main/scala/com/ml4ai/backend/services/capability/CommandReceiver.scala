package com.ml4ai.backend.services.capability

trait CommandReceiver {

  def getSupport(): String

  def execute(command: Command): Unit

}