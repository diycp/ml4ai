package com.ml4ai.backend.services.providers

import com.google.gson.{Gson, GsonBuilder, JsonParser}
import java.util._

import com.ml4ai.backend.consts.UserConstants
import com.ml4ai.backend.services.{DbMapService, RabbitmqService, UserService}
import com.ml4ai.backend.services.capability._
import com.ml4ai.core.stack.elasticsearch.ElasticSearchAgent
import com.ml4ai.core.stack.mq.RabbitMQAgent
import com.ml4ai.core.stack.utils.Toolkit
import javax.annotation.PostConstruct
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.{Autowired, Value}
import org.springframework.stereotype.Service

@Service
class LoginCommandReceiver extends CommandReceiver {
  @Value("${elasticsearch.servers}") private val esServers: String = null
  @Value("${elasticsearch.cluster}") private val cluster: String = null
  @Autowired var rabbitmqService: RabbitmqService = null
  @Autowired var userService: UserService = null
  @Autowired var dbMapService: DbMapService = null
  private var elasticsearch: ElasticSearchAgent = null
  private var rabbitMQAgent: RabbitMQAgent = null
  private var googleJsonWrapperBean: Gson = null
  private var jsonParser: JsonParser = null

  @PostConstruct def init(): Unit = {
    elasticsearch = ElasticSearchAgent.builder.esServers(esServers).esCluster(cluster).build
    rabbitMQAgent = rabbitmqService.getRabbitMQAgent
    jsonParser = new JsonParser
    val googleJsonWrapperBuilder = new GsonBuilder
    googleJsonWrapperBuilder.serializeNulls
    googleJsonWrapperBuilder.setDateFormat("yyyy-MM-dd")
    googleJsonWrapperBean = googleJsonWrapperBuilder.create
  }

  override def execute(command: Command): Unit = {
    val username = command.get("username")
    val password = command.get("password")
    if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password)) {
      val user = userService.findByLogin(username)
      if (password == Toolkit.MD5.getMessageDigest(user.getPassword())) {
        val tokenStoreMap = dbMapService.generateLocalMapWrapper(UserConstants.TOKEN_USER_MAP)
        val tokenKey = Toolkit.StringHelper.uuid
        tokenStoreMap.put(tokenKey, username)
        val response = new HashMap[String, AnyRef]
        response.put("code", "200")
        response.put("msg", "认证成功!")
        response.put("data", tokenKey)
        rabbitMQAgent.produceText("", command.getCallback, googleJsonWrapperBean.toJson(response), false)
      } else {
        val response = new HashMap[String, AnyRef]
        response.put("code", "401")
        response.put("msg", "认证失败")
        response.put("data", "")
        rabbitMQAgent.produceText("", command.getCallback, googleJsonWrapperBean.toJson(response), false)
      }
    } else {
      val response = new HashMap[String, AnyRef]
      response.put("code", "401")
      response.put("msg", "认证失败")
      response.put("data", "")
      rabbitMQAgent.produceText("", command.getCallback, googleJsonWrapperBean.toJson(response), false)
    }
  }

  override def getSupport(): String = "login"
}
