package com.ml4ai.backend.services.providers

import com.ml4ai.backend.services.RabbitmqService
import com.ml4ai.backend.services.actors.anys.FileStreamTaskStart
import com.ml4ai.backend.services.capability._
import com.ml4ai.core.stack.parallel.RootControlActor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CreateStreamCommandReceiver extends CommandReceiver {

  @Autowired var rabbitmqService: RabbitmqService = null

  override def execute(command: Command): Unit = {
    val schedule = RootControlActor("root")
    schedule("file2queue") ! FileStreamTaskStart(command.getCallback, Map("rabbitMQAgent" -> rabbitmqService.getRabbitMQAgent, "queue" -> command.getCallback, "file" -> ""))
  }

  override def getSupport() = "create.stream"
}
