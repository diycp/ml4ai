package com.ml4ai.backend.services.providers

import com.ml4ai.backend.services.RabbitmqService
import com.ml4ai.backend.services.actors.anys.FileStreamTaskStop
import com.ml4ai.backend.services.capability._
import com.ml4ai.core.stack.mq.RabbitMQAgent
import com.ml4ai.core.stack.parallel.RootControlActor
import javax.annotation.PostConstruct
import org.springframework.beans.factory.annotation.{Autowired, Value}
import org.springframework.stereotype.Component

@Component
class CloseMessageCommandReceiver extends CommandReceiver {

  @Autowired var rabbitmqService: RabbitmqService = null

  var rabbitMQAgent: RabbitMQAgent = null

  @PostConstruct
  def init(): Unit = {
    rabbitMQAgent = rabbitmqService.getRabbitMQAgent
  }

  override def getSupport(): String = "remove.queue"

  override def execute(command: Command): Unit = {
    val queue = command.get("queue")
    val schedule = RootControlActor("root")
    schedule("file2queue") ! FileStreamTaskStop(queue)
    println(s"关闭任务：${queue}")
  }

}
