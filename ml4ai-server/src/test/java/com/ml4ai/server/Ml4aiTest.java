package com.ml4ai.server;

import cn.edu.hfut.dmic.webcollector.model.CrawlDatum;
import com.gargoylesoftware.htmlunit.ScriptException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.JavaScriptErrorListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ml4ai.backend.Boot;
import com.ml4ai.backend.services.DbMapService;
import com.ml4ai.backend.services.EsService;
import com.ml4ai.backend.services.WebCatchService;
import com.ml4ai.backend.stack.java.Ml4aiClassloader;
import com.ml4ai.backend.utils.*;
import com.ml4ai.core.stack.mq.RabbitMQAgent;
import com.ml4ai.core.stack.utils.GsonUtil;
import com.ml4ai.core.stack.utils.Toolkit;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.function.Function;

/**
 * Created by leecheng on 2018/10/20.
 */
@Slf4j
//@SpringBootTest(classes = Boot.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@RunWith(SpringRunner.class)
public class Ml4aiTest {

    @Autowired
    EsService esService;

    @Autowired
    WebCatchService webCatchService;

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    WebApplicationContext webApplicationContext;

    @Value("${rabbitmq.host}")
    private String rabbitmqHost;

    @Value("${rabbitmq.port}")
    private Integer rabbitmqPort;

    @Value("${rabbitmq.username}")
    private String rabbitmqUsername;

    @Value("${rabbitmq.password}")
    private String rabbitmqPassword;

    @Value("${rabbitmq.virtualHost}")
    private String rabbitmqVirtualHost;

    Gson gson;

    JsonParser jsonParser;

    @Before
    public void before() {
        System.out.println("开始测试+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        gsonBuilder.setDateFormat("yyyy-MM-dd");
        gson = gsonBuilder.create();
        jsonParser = new JsonParser();
    }

    @Test
    public void testHashMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("1", "A");
        map.put("2", "B");

        Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator();

        System.out.println(it.hasNext());
        System.out.println(it.next());

        //map.put("3", "C");
        //map.put("4", "D");
        //map.put("5", "E");

        System.out.println(it.hasNext());
        System.out.println(it.next());

        //System.out.println(it.hasNext());
        //System.out.println(it.next());

        // System.out.println(it.next());

    }

    public void esTest() {
        Map<String, Object> doc = new LinkedHashMap<>();
        doc.put("data", "hello");
        log.info("{}", esService.putJson("test", "testEs", StringHelper.uuid(), doc));
    }

    public void delete() {
        esService.deleteDocument("test", "testEs", "4122066c6041442b86d83a8390fa2867");
    }

    public void testWebCatchService() {
        webCatchService.catchPage("1", Arrays.asList("https://www.hao123.com/"), URLUtil.urlToMatch("https://www.hao123.com/") + ".*");
    }

    public void testRedis() {

        HashOperations hashOperations = redisTemplate.opsForHash();
        //写入一个值
        hashOperations.entries("test").put("1", "2");
        hashOperations.put("test", "2", "3");
        //读取一个值
        log.info("{}", hashOperations.entries("test").get("1"));
        log.info("{}", hashOperations.entries("test").get("2"));
        log.info("{}", hashOperations.get("test", "1"));
        log.info("{}", hashOperations.get("test", "2"));


    }

    public void testSerializer() {
        log.info(new Gson().toJson(new CrawlDatum()));
    }

    @SneakyThrows
    public void testWebClient() {
        WebClient client = new WebClient();
        client.setJavaScriptErrorListener(new JavaScriptErrorListener() {
            @Override
            public void scriptException(HtmlPage htmlPage, ScriptException e) {
                e.printStackTrace();
            }

            @Override
            public void timeoutError(HtmlPage htmlPage, long l, long l1) {
                log.info(htmlPage.asXml());
            }

            @Override
            public void malformedScriptURL(HtmlPage htmlPage, String s, MalformedURLException e) {
                log.error("{}", e);
            }

            @Override
            public void loadScriptError(HtmlPage htmlPage, URL url, Exception e) {
                e.printStackTrace();
            }
        });
        client.getOptions().setThrowExceptionOnScriptError(false);
        client.getOptions().setJavaScriptEnabled(true);
        client.getOptions().setCssEnabled(false);
        client.getOptions().setRedirectEnabled(true);
        HtmlPage page = client.getPage("https://www.hao123.com/");
        client.waitForBackgroundJavaScript(10000);
        System.out.println(page.asXml());
    }

    @SneakyThrows
    public void testWeb() {
        MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    public void test1() {
        Map<String, String> map = SpringUtils.getService(DbMapService.class).generateLocalMapWrapper("1");
        map.put("1", "2");
        System.out.println(map.get("1"));
        System.out.println(map.keySet());
        System.out.println(map.entrySet());
    }

    @SneakyThrows
    public void testClass() {
        Function function = x -> {
            try {
                return ObjUtil.str2obj(ObjUtil.obj2str(x));
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        };

        String className = function.getClass().getName();
        byte[] bytes = IOUtils.toByteArray(function.getClass().getResourceAsStream(className));
        System.out.println(function.getClass().getName());
        System.out.println(bytes);
        System.out.println(new String(bytes));
        IOUtils.write(bytes, new FileWriter("d:/doc/data/cls"));
    }

    @SneakyThrows
    public void testClassLoader() {
        ClassLoader classLoader = new Ml4aiClassloader((name) -> {
            try {
                return IOUtils.toByteArray(new FileInputStream("d:/doc/data/cls"));
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }, Thread.currentThread().getContextClassLoader());
        //Class<?> c = classLoader.loadClass("com.ml4ai.server.Ml4aiTest$$Lambda$1684/497164700");
        Class<?> c = classLoader.loadClass("com.ml4ai.server.Ml4aiTest$$Lambda$1684");
        System.out.println(c);
    }

    @SneakyThrows
    public void a1() {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", "127.0.0.1:9092");
        properties.put("acks", "all");
        properties.put("retries", 0);
        properties.put("batch.size", 16384);
        properties.put("linger.ms", 1);
        properties.put("buffer.memory", 33554432);
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        Producer<String, String> producer = null;
        try {
            producer = new KafkaProducer<>(properties);
            for (int i = 0; i < 100; i++) {
                String msg = "Message " + i;
                producer.send(new ProducerRecord<>("test", msg));
                System.out.println("Sent:" + msg);
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            producer.close();
        }
    }

    @SneakyThrows
    public void a2() {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", "127.0.0.1:9092");
        properties.put("group.id", "group-1");
        properties.put("enable.auto.commit", "true");
        properties.put("auto.commit.interval.ms", "1000");
        properties.put("auto.offset.reset", "earliest");
        properties.put("session.timeout.ms", "30000");
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        ConsumerConfig conf = new ConsumerConfig(properties);

        KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<>(properties);
        kafkaConsumer.subscribe(Arrays.asList("TEST".toLowerCase()));
        while (true) {
            ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.of(1, ChronoUnit.MINUTES));
            for (ConsumerRecord<String, String> record : records) {
                System.out.printf("offset = %d, value = %s", record.offset(), record.value());
                System.out.println();
            }
        }
    }

    @SneakyThrows
    public void testRabbitmqProducer() {
        RabbitMQAgent mq = RabbitMQAgent.builder().host("localhost").port(5672).user("admin").password("admin").vHost("/").build();
        mq.declareQueue("TEST", true, false, false, null);
        for (int i = 0; i < 10; i++) {
            String message = "Hello number " + i;
            mq.produceText("", "TEST", message, true);
            log.info("Producer Send {}", message);
        }
    }

    @SneakyThrows
    public void testRabbitmqConsumer() {
        RabbitMQAgent mq = RabbitMQAgent.builder().host("localhost").port(5672).user("admin").password("admin").vHost("/").build();
        mq.consumeText("TEST", 1, text -> {
            log.info("收到消息：{}", text);
            return false;
        });
        Thread.sleep(10000L);
    }

    public void testHBase() {
        /**
         try {
         Configuration conf = HBaseConfiguration.create();
         conf.set("hbase.zookeeper.quorum", "127.0.0.1");
         conf.set("hbase.zookeeper.property.clientPort", "2181");
         org.apache.hadoop.hbase.client.Connection connection = org.apache.hadoop.hbase.client.ConnectionFactory.createConnection(conf);
         Admin admin = connection.getAdmin();
         TableDescriptorBuilder tableDescriptorBuilder = TableDescriptorBuilder.newBuilder(TableName.valueOf("test"));
         TableDescriptor tableDescriptor = tableDescriptorBuilder.build();
         admin.createTable(tableDescriptor);
         } catch (Exception e) {
         e.printStackTrace();
         }
         */
    }

    @SneakyThrows({IOException.class})
    public void testHDFS() {
        Configuration conf = new Configuration();
        conf.set("fs.defaultFS", "hdfs://master:8020");
        FileSystem fs = FileSystem.get(conf);
        long start = Clock.systemUTC().millis();
        RemoteIterator<LocatedFileStatus> iterator = fs.listFiles(new Path("/"), false);
        while (iterator.hasNext()) {
            LocatedFileStatus status = iterator.next();
            log.info(status.getPath().toUri().getPath());
        }
        long end = Clock.systemUTC().millis();
        log.info("总共花{}毫秒", end - start);
    }

    @SneakyThrows
    public void testPublish() {
        BlockingQueue<String> block = new ArrayBlockingQueue<>(1);
        RabbitMQAgent rabbitMQAgent = RabbitMQAgent.builder().host(rabbitmqHost).port(rabbitmqPort).user(rabbitmqUsername).password(rabbitmqPassword).vHost(rabbitmqVirtualHost).build();
        String callbackQueue = Toolkit.StringHelper.uuid();
        rabbitMQAgent.declareQueue(callbackQueue, false, false, true, null);
        rabbitMQAgent.consumeText(callbackQueue, 1, body -> {
            block.offer(body);
            return true;
        });
        JsonObject command = new JsonObject();
        command.addProperty("command", "login");
        command.addProperty("username", "admin");
        command.addProperty("password", MD5.getMessageDigest("admin".getBytes()));
        command.addProperty("queue", callbackQueue);
        rabbitMQAgent.produceText("", "command", gson.toJson(command), false);

        String response = block.take();
        log.info("认证：{}", response);
        JsonObject responseEntity = jsonParser.parse(response).getAsJsonObject();

        String token = GsonUtil.getFieldAsString(responseEntity, "data");

        Map<String, Object> m = new HashMap<>();
        m.put("command", "publicContent");
        m.put("token", token);
        m.put("queue", callbackQueue);
        Map<String, Object> publishObj = new HashMap<>();
        publishObj.put("name", "自然科学");
        publishObj.put("introduce", "自然科学，一切的基础");
        publishObj.put("date", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        publishObj.put("content", "自然科学");
        m.put("parameter", publishObj);
        rabbitMQAgent.produceText("", "command", gson.toJson(m), false);
        response = block.take();
        log.info("发布结果：{}", response);

        Thread.sleep(10000);
    }

    @After
    public void after() {
        System.out.println("结束测试+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
    }

}
