import React from 'react';
import { XTree } from '../common/items';
import { api, ajax } from '../../config.service';
import { Table, Col, Button, Tabs, Modal, Form, Input, Icon, Popconfirm, message } from 'antd';

const FormItem = Form.Item;

//菜单组件
class MenuComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectData: undefined
        };
    }

    select(me, selectedKeys, e) {
        if (e.selected) {
            me.setState({
                selectData: e.node.props.data
            });
        } else {
            me.setState({
                selectData: undefined
            });
        }
    }

    render() {
        return (
            <div style={{ width: "100%", height: "100%", position: "relative" }}>
                <div style={{ position: "absolute", left: "0px", top: "0px", botton: "0px", width: "300px", height: "100%", borderRight: "1px solid #CCC" }}>
                    <XTree registry={(subComponent) => {
                        this.treePane = subComponent;
                    }} properties={{ autoExpandParent: true, defaultExpandAll: true, onSelect: (selectedKeys, e) => { this.select(this, selectedKeys, e) } }} apiURL={api.menuDataURL} keyField="id" valueField="menuName"></XTree>
                </div>
                <div style={{ position: "absolute", left: "300px", top: "0px", right: "0px", bottom: "0px", overflow: "hidden", paddingLeft: "10px" }}>
                    <OperationPane parent={this} registry={(subComponent) => { this.operationPane = subComponent; }} selectData={this.state.selectData} />
                </div>
            </div>
        );
    }
}

//操作组件
class OperationPane extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            selectData: props.selectData,
            title: undefined,
            visible: false,
            formData: undefined
        };
        if (props.registry) {
            props.registry(this);
        }
    }

    componentDidMount() {
        this.refreshData();
    }

    componentWillReceiveProps(nextProps) {
        this.props = nextProps;
        this.refreshData();
    }

    refreshData() {
        let me = this;
        let data;
        let selectData = this.props.selectData;
        let title;
        if (selectData) {
            title = '[' + selectData.menuName + ']子菜单';
            data = { parentId: selectData.id };
        } else {
            title = '顶级菜单';
            data = { parentIsNull: true };
        }
        ajax.postJson(api.menuQuery, data, (response) => {
            me.setState({
                title: title,
                data: response.data,
                visible: false,
                formData: undefined
            });
        });
    }

    render() {
        let me = this;
        const DataForm = Form.create({})(CustomizedForm);
        return (<div>
            <h1>{this.state.title}</h1>
            <Table pagination={false} dataSource={this.state.data}>
                <Col
                    title="ID"
                    dataIndex="id"
                    key="id"
                />
                <Col
                    title="菜单名"
                    dataIndex="menuName"
                    key="menuName"
                />
                <Col
                    title="数据"
                    dataIndex="menuData"
                    key="menuData"
                />
                <Col title="操作" key="action" render={(record) => {
                    return (<div><a onClick={
                        () => {
                            console.log(record);
                            me.setState({ visible: true, formData: record });
                        }
                    }>编缉</a>&nbsp;
                        <Popconfirm title="你确认要删除菜单吗" onConfirm={() => {
                            let data = {
                                id: record.id
                            };
                            ajax.postJson(api.menuDelete, data, (response) => {
                                me.props.parent.treePane.loadData();
                                me.refreshData();
                            });
                        }} onCancel={() => { }} okText="是" cancelText="否">
                            <a>删除</a>
                        </Popconfirm>
                    </div>);
                }} />
            </Table>
            <div>
                <Button type="primary" style={{ margin: "10px" }} onClick={() => { this.setState({ visible: true, formData: undefined }); }}>添加</Button>
                <Modal okText="确定" onOk={() => {
                    let me = this;
                    let data = this.childrenForm.getFieldsValue();
                    let dataId = data.formData ? data.formData.id : null;
                    let parentId = this.props.selectData ? this.props.selectData.id : null;
                    let formData = data.fieldsValue;
                    formData['id'] = dataId;
                    formData['parentId'] = parentId;
                    ajax.postJson(api.menuSave, formData, (response) => {
                        me.props.parent.treePane.loadData();
                        me.refreshData();
                    });
                }} cancelText="取消" onCancel={() => { this.setState({ visible: false }) }} visible={this.state.visible} >
                    <DataForm registry={(subComponent) => { this.childrenForm = subComponent; }} formData={this.state.formData} />
                </Modal>
            </div>
        </div >);
    }
}


class CustomizedForm extends React.Component {

    constructor(props) {
        super(props);
        if (props.registry) {
            props.registry(this);
        }
    }

    getFieldsValue() {
        return { formData: this.props.formData, fieldsValue: this.props.form.getFieldsValue() };
    }

    componentDidMount() {
        this.props.form.setFieldsValue(this.props.formData);
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form>
                <FormItem label="菜单名称">
                    {getFieldDecorator('menuName', {
                        rules: [{ required: true, message: '请输入菜单名' }],
                    })(
                        <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="菜单名" />
                    )}
                </FormItem>
                <FormItem label="菜单数据">
                    {getFieldDecorator('menuData', {
                        rules: [{ required: true, message: '请输入菜单数据' }],
                    })(
                        <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="菜单数据" />
                    )}
                </FormItem>
                <FormItem label="菜单详细">
                    {getFieldDecorator('details', {
                        rules: [],
                    })(
                        <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="菜单详细" />
                    )}
                </FormItem>
            </Form>
        );
    }
}

export { MenuComponent };